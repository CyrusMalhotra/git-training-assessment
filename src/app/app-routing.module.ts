import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PlanComponent } from './plan/plan.component';
import { PocComponent } from './poc/poc.component';

const routes: Routes = [
  { path:'' , component: HomeComponent },
  { path:'plan' , component: PlanComponent },
  { path:'poc' , component: PocComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
